class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary
    };
    get name() {
        return this._name
    };
    set name(employeeName) {
        if (!Number.isNaN(+employeeName)) {
            this._name = employeeName;
        }
    };
    get age() {
        return this._age
    };
    set age(employeeAge) {
        if (employeeAge > 18) {
            this._age = employeeAge;
        }
    };
    get salary() {
        return this._salary
    };
    set salary(employeeSalary) {
        if (Number.isInteger(+employeeSalary)) {
            this._salary = employeeSalary;
        }
    };
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    get salary() {
        return this._salary * 3
    };
    set salary(employeeSalary) {
        if (Number.isInteger(+employeeSalary)) {
            this._salary = employeeSalary;
        }
    }
    get lang () {
        return this._lang 
    };
    set lang (employeeLang) {
        if (!Number.isNaN(+employeeLang) && employeeLang.length >= 2 ) {
            this._lang = employeeLang;
        }
    }
}

    const frontEndProgrammer = new Programmer('Nazar', 20, 2000, 'Russ');
console.log(frontEndProgrammer);
    const backEndProgrammer = new Programmer('David', 30, 4000, 'Eng');
console.log(backEndProgrammer);
    const fullStackProgrammer = new Programmer('Vlad', 25, 3000, 'Uk');
console.log(fullStackProgrammer);
