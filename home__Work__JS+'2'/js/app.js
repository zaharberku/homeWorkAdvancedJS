const arrProperty = ['author', 'name', 'price'];
const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];

const arrError = [
  {
    nameProperty: 'name',
    nameError: 'LackOfPropertyName',
    message: 'You did not give name of the book'
  },
  {
    nameProperty: 'price',
    nameError: 'LackOfPropertyPrice',
    message: 'You did not give price'
  },
  {
    nameProperty: 'author',
    nameError: 'LackOfPropertyAuthor',
    message: 'You did not give author'
  },
];

class List {
  constructor(name, price, author) {
    this.name = name;
    this.price = price;
    this.author = author;
  }
  render() {
    this.createElement()
  }
  createElement() {
    const block = document.getElementById('root');
    block.insertAdjacentHTML('afterbegin', `
    <div class ="block__price">
    <h1 class ="block__title-text">${this.name}</h1>
    <ul>
       <li>${this.author}</li>
       <li>$${this.price}</li>
    </ul>
    </div>
    `)
  }
};

class newError extends Error {
  constructor(name, message, serialNumber) {
    super()
    this.name = name
    this.message = message
    this.serialNumber = `serial number object: ${serialNumber + 1}`
  }
};



const getProperties = (elem) => {
  const createList = new List(elem.name, elem.price, elem.author).render()
};



const validObjectProperty = (arr, error) => {
  arr.forEach((element, index) => {
    const nameProperty = arrProperty.filter(elem => !element.hasOwnProperty(elem)).join('')
    error.forEach(elemError => {
        try {
          if (nameProperty === elemError.nameProperty) {
            throw new newError(elemError.nameError, elemError.message, index)
          }
        } catch (err) {
          console.log(err.name);
          console.log(err.message);
          console.log(err.serialNumber);
        }
      })
      if (nameProperty === '') {
        getProperties(arr[index])
      }
  })
}



validObjectProperty(books, arrError);