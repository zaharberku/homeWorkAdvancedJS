const property = ['author', 'name', 'price'];
const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];
class newError extends Error {
    constructor(name, message, serialNumber) {
      super()
      this.name = name
      this.message = message
      this.serialNumber = `serial number object: ${serialNumber + 1}`
    }
  };
  

const errors = [
  {
    nameProperty: 'name',
    nameError: 'LackOfPropertyName',
    message: 'You did not give name of the book'
  },
  {
    nameProperty: 'price',
    nameError: 'LackOfPropertyPrice',
    message: 'You did not give price'
  },
  {
    nameProperty: 'author',
    nameError: 'LackOfPropertyAuthor',
    message: 'You did not give author'
  },
];

class List {
  constructor(name, price, author) {
    this.name = name;
    this.price = price;
    this.author = author;
  }
  render() {
    this.createElement()
  }
  createElement() {
    const block = document.getElementById('root');
    block.insertAdjacentHTML('afterbegin', `
    <div class ="block__price">
    <h1 class ="block__title-text">${this.name}</h1>
    <ul>
       <li>${this.author}</li>
       <li>$${this.price}</li>
    </ul>
    </div>
    `)
  }
};


const getProperties = (elem) => {
  const createList = new List(elem.name, elem.price, elem.author).render()
};

function callError(variable, arrErrors, i) {
  arrErrors.forEach(element => {
    if (variable === element.nameProperty) {
      throw new newError(element.nameError, element.message, i)
    }
  })
}

function validObjectProperty(elem, arrErrors, arrProperty, i) {
    arrProperty.forEach(element =>{
      if (!elem.hasOwnProperty(element)) {
        return callError(element, arrErrors, i)
      }
    })
  getProperties(elem)
}

function errorСhecking(arrBooks, arrErrors, arrProperty ) {
arrBooks.forEach((element,index) =>{
    try{
    return  validObjectProperty(element, arrErrors, arrProperty, index)
    }catch(e){
     console.log(e.name);
     console.log(e.message);
     console.log(e.serialNumber);
    }
  })
}

errorСhecking(books, errors, property)
// function name4() {
  
// }
// name4(arrProperty, name3())

// const books = [
//   {
//     author: "Скотт Бэккер",
//     name: "Тьма, что приходит прежде",
//     price: 70,
//   },
//   {
//     author: "Скотт Бэккер",
//     name: "Воин-пророк",
//   },
//   {
//     name: "Тысячекратная мысль",
//     price: 70,
//   },
//   {
//     author: "Скотт Бэккер",
//     name: "Нечестивый Консульт",
//     price: 70,
//   },
//   {
//     author: "Дарья Донцова",
//     name: "Детектив на диете",
//     price: 40,
//   },
//   {
//     author: "Дарья Донцова",
//     name: "Дед Снегур и Морозочка",
//   },
// ];

// const verifiableProperties = ["name", "author", "price"];
// const container = document.querySelector("#root");

// function checkProperties(obj) {
//   for (const property of verifiableProperties) {
//     if (!(property in obj)) {
//       throw new Error(
//         `В обьекте ${JSON.stringify(obj)} отсутствует Свойство "${property}"`
//       );
//     }
//   }
//   return true;
// }

// function getBooks() {
//   return books.filter((element) => {
//     try {
//       return checkProperties(element);
//     } catch (error) {
//       console.log(error.message);
//     }
//   });
// }

// function createBookList(parent, array) {
//   const ul = document.createElement("ul");
//   parent.appendChild(ul);
//   array.forEach((book) => {
//     ul.insertAdjacentHTML(
//       "beforeend",
//       `<li style="margin-bottom: 15px">${book.author}: "${book.name}" <div>Цена: ${book.price} грн.</div></li>`
//     );
//   });
// }

// createBookList(container, getBooks());








