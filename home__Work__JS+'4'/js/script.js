class CreateElement {
    constructor({ className, dataElement }) {
        this.className = className;
        this.dataElement = dataElement
        this.element;
    }
    render(container, place) {
        this.createElement(this.dataElement);
        this.addPage(container, place);
    }
    createElement({ name, attribut }) {
        this.element = document.createElement(name)
        this.element.className = this.className.join('')
        Object.keys(attribut).forEach(keys => {
            this.element[keys] = attribut[keys]
        })
    }
    addPage(container, place) {
        container.insertAdjacentHTML(place, this.element.outerHTML)
    }
}

class Div extends CreateElement {
    constructor({ className, divData: { p, h1, span } }) {
        super({ className })
        this.p = p;
        this.h1 = h1;
        this.span = span;
    }
    render(container, place) {
        this.createDiv();
        this.addFields();
        this.addPage(container, place);
    }
    createDiv() {
        this.createElement({
            name: 'div',
            attribut: {
                style: 'display:block'
            }
        })
    }
    addFields() {
        this.h1.forEach(element => element.render(this.element, 'beforeend'))
        this.span.forEach(element => element.render(this.element, 'beforeend'))
        this.p.forEach(element => element.render(this.element, 'beforeend'))
    }
}





const requestURL = 'https://ajax.test-danit.com/api/swapi/films'
let index = 0

function addOnPage(charactersArr) {
    const allDiv = document.querySelectorAll('div')
    allDiv[index].insertAdjacentHTML('beforeend', `<p><b>Персонажи:</b> ${charactersArr.join(', ')}</p>`)
    index++
}

function getCharacters(charactersArr) {
    charactersArr.forEach(element => {
        Promise.all(element.map(link => {
            return fetch(link)
                .then(response => response.json())
                .then(data => data.name)
        }))
            .then(addOnPage)
    })
}

function createElementAndAddPage(name, episodeId, openingCrawl, id) {
    const body = document.querySelector('body')
    const div = new Div({
        className: [`div-${id}`],
        divData: {
            p: [
                new CreateElement({
                    className: [`p-${id}`],
                    dataElement: {
                        name: 'p',
                        attribut: {
                            innerHTML: `<b>Описание:</b>${openingCrawl}`,
                        }
                    }
                })
            ],
            h1: [
                new CreateElement({
                    className: [`h1-${id}`],
                    dataElement: {
                        name: 'h1',
                        attribut: {
                            innerHTML: `<b>Название фильма:</b>${name}`,
                        }
                    }
                })
            ],
            span: [
                new CreateElement({
                    className: [`span-${id}`],
                    dataElement: {
                        name: 'span',
                        attribut: {
                            innerHTML: `<b>Эпизод:</b>${episodeId}`,
                        }
                    }

                })
            ]
        }
    })
    div.render(body, 'beforeend')
}

function getData(arrData) {
    return arrData.map(element => {
        const { characters, name, episodeId, openingCrawl, id } = element
        createElementAndAddPage(name, episodeId, openingCrawl, id)
        return characters
    })
}

fetch(requestURL)
    .then(response => response.json())
    .then(getData)
    .then(getCharacters)
    .catch(console.log)