const btn = document.querySelector('.btn')

function addOnPage({city,country,regionName,countryCode,region}){
const ul = document.createElement('ul')
ul.className = 'ul'
ul.innerHTML = `
<li>City:${city}</li>
<li>Country:${country}</li>
<li>RegionName:${regionName}</li>
<li>CountryCode:${countryCode}</li>
<li>Region:${region}</li>`
btn.after(ul)
}

async function getDataLocation(ip){
    const data = await fetch(`http://ip-api.com/json/${ip}`).then(response=>response.json());
    addOnPage(data);
}

async function getIpAddress(){
    btn.setAttribute('disabled','disabled')
    const {ip} = await fetch('https://api.ipify.org/?format=json').then(response=> {
    btn.removeAttribute('disabled')
    return response.json()
    });
    getDataLocation(ip);
} 


btn.addEventListener('click',getIpAddress)