export default class Fetch {
    static async getData(url){
       return await fetch(`https://ajax.test-danit.com/api/json/${url}`)
        .then(resolve => resolve.json())
        .then(data => data)
    } 
    static async deleteData(postId){
      return await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`,{method:'DELETE'})
     
    }
}


