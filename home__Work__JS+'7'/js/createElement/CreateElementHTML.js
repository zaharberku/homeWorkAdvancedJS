export default class CreateElementHTML{
    constructor(className = '',id = ''){
        this.className = className;
        this.id = id;
        this.element = null;
    }
    render(container,place){
        this.createElement()
        this.addOnPage(container,place)
    }
    createElement({name, attributes}){
        this.element = document.createElement(name)
        if (this.className) {
            this.element.className = this.className.join(' ')  
        }
        if (this.id){
        this.element.id = this.id 
        }
        Object.keys(attributes).forEach(key=>{
            this.element[key] = attributes[key]
        })
    }
    addOnPage(container,place){
        document.querySelector(container).insertAdjacentHTML(place, this.element.outerHTML)
    }
}