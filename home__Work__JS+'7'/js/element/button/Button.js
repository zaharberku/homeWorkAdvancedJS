import CreateElementHTML from "../../createElement/CreateElementHTML.js";
import Fetch from "../../api/Fetch.js";

export default class Button extends CreateElementHTML{
    constructor({className,id, type, text=''}){
        super(className,id)
        this.type = type;
        this.text = text;
    }
    render(container,place){
        this.createButton()
        this.addOnPage(container,place)
        this.lisenerBtnClose(container)
    }
    createButton(){
        this.createElement({
            name:'button',
            attributes:{
                type:this.type,
                innerHTML:this.text,
            }
        })
    }
    async closeBlock(event){
        const element = event.currentTarget
        const NUMBER_FROM_DELETE_CARD = +event.currentTarget.id.slice(-1)
        const result = await Fetch.deleteData(NUMBER_FROM_DELETE_CARD)
        if(result.status < 400){
            element.remove()
        }
    }
    lisenerBtnClose(name){
        const div = document.querySelector(name)
        div.addEventListener('click', (event) => {
        if (event.target.tagName === 'BUTTON') {
            this.closeBlock(event)
        }    
        })
    }
}