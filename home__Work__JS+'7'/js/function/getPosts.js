import Fetch from "../api/Fetch.js";
import CreatePosts from '../posts/CreatePosts.js'


async function getPosts(arrDataUser) {
    const data = 'posts'
    const dataPost = await Fetch.getData(data)
    dataPost.forEach(element => {
        const { body, title, userId, id } = element
        arrDataUser.find(user=>{
            if (user.includes(userId)) {
               const [name,email] = user 
               const post = new CreatePosts({
                className:['post','w-25','h-25',"card",'p-2','position-relative'],
                id:`post-${id}`,
                body,
                title,
                name,
                email
               })
               post.render('.container')
            }
        })
    })
}

export default getPosts
