import Fetch from "../api/Fetch.js";
import getPosts from './getPosts.js'

async function getUsers() {
    const data = 'users'
    const dataUsers = await Fetch.getData(data)
    const arrWithDataUser = dataUsers.map(element => {
        const { name, email, id } = element
        return [name, email, id]
    })
    getPosts(arrWithDataUser);
}

export default getUsers