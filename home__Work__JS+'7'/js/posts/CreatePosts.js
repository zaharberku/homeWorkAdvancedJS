import CreateElementHTML from "../createElement/CreateElementHTML.js";
import Button from "../element/button/Button.js";

export default class CreatePosts extends CreateElementHTML{
    constructor({className,id, title, body, name, email}){
        super(className,id)
        this.title = title;
        this.body = body;
        this.name = name;
        this.email = email;
    }
    render(container = 'body', place = 'beforeend'){
        this.createPorst()
        this.addOnPage(container, place)
        this.createBtnClose()
    }
    createPorst(){
        this.createElement({
            name:'div',
            attributes:{
                innerHTML:`
                <img class="img rounded-circle" src="./img/avatar.png" alt="avatar">
                <div class="name-user d-flex">
                <h2>${this.name}</h2>
                <a href="mailto:${this.email}">${this.email}</a>
                </div>
                <div class="post-user">
                <h1 class="card-title">${this.title}</h1>
                <p class="card-text">${this.body}</p>
                </div>
                `
            }
        })
    }
    createBtnClose(){
        const btnClose = new Button({
            className:['btn-close',"position-absolute", 'top-0', 'end-0'],
            type:'button'
        })
        btnClose.render(`#${this.id}`,'afterbegin')
    }
}